//
//  VideoPlayerView.swift
//  VtLibraryTour
//
//  Created by Jonathan Bradley on 6/21/16.
//  Copyright © 2016 jonathanbradley@vt.edu. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

protocol VideoPlayerViewDelegate {
    func videoPlayerViewUpdateButton( videoPlayerView: VideoPlayerView )
}

class VideoPlayerView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        print("The video box has appeared!")
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var currentMoviePath = appDelegate.moviePath
        print(currentMoviePath)
        
        let player = AVPlayer(URL: currentMoviePath)
        let playerController = AVPlayerViewController()
        playerController.player = player
        //playerController.showsPlaybackControls = false
        self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        playerController.view.frame = self.view.frame
        
        player.play()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(VideoPlayerView.playerDidFinishPlaying(_:)), name: AVPlayerItemDidPlayToEndTimeNotification, object: player.currentItem)
        
    }
    
    func playerDidFinishPlaying(notification: NSNotification) {
       print("AVPlayer finished playing")
       NSNotificationCenter.defaultCenter().postNotificationName("videoFinished", object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
