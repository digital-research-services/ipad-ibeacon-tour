//
//  CoverImageController.swift
//  VtLibraryTour
//
//  Created by Jonathan Bradley on 6/21/16.
//  Copyright © 2016 jonathanbradley@vt.edu. All rights reserved.
//

import UIKit

class CoverImageController: UIViewController {

    @IBOutlet weak var searching: UIImageView!
    @IBOutlet weak var found: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.switchImages), name:switchImage, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        //print("The cover image has appeared!")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func switchImages() {
        if searching.hidden == false {
        searching.hidden = true
        found.hidden = false
        } else {
            searching.hidden = false
            found.hidden = true
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
