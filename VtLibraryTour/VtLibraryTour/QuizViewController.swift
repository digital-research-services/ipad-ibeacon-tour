//
//  QuizViewController.swift
//  VtLibraryTour
//
//  Created by Jonathan Bradley on 8/18/16.
//  Copyright © 2016 jonathanbradley@vt.edu. All rights reserved.
//

import UIKit

var blue = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
var clear = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 0)


class QuizViewController: UIViewController {

    @IBOutlet weak var image4: UIButton!
    @IBOutlet weak var image3: UIButton!
    @IBOutlet weak var image2: UIButton!
    @IBOutlet weak var image1: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var possibility1: UIButton!
    @IBOutlet weak var possibility5: UIButton!
    @IBOutlet weak var possibility4: UIButton!
    @IBOutlet weak var possibility3: UIButton!
    @IBOutlet weak var possibility2: UIButton!

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var answer6: UILabel!
    @IBOutlet weak var answer5: UILabel!
    @IBOutlet weak var answer4: UILabel!
    @IBOutlet weak var answer3: UILabel!
    @IBOutlet weak var answer2: UILabel!
    @IBOutlet weak var answer1: UILabel!
    @IBOutlet weak var explanation: UILabel!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var question: UILabel!
    
    
    var currentQuestion = JSON("{}")
    var currentQuestionNumber = 1
    var x = ""
    var json = JSON("{}")
    var sectionLength = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let jsonData = NSData(contentsOfFile: jsonLocalPath.path!) {
            json = JSON(data: jsonData)
            if String(currentStop-1) == "2" {
                x = "2"
            } else if String(currentStop-1) == "4" {
                x = "4"
            } else if String(currentStop-1) == "6" {
                x = "6"
            }
            
            sectionLength = json[x].count
           
            print(sectionLength)
            buildQuiz(x, y: currentQuestionNumber-1, json: json)
            
            currentQuestion = json
            
        } else {
            print("json didn't load into quiz.")
        }
        
        // Do any additional setup after loading the view.
        
    }
    
    var matchOrder = 0
    var answerArray = [UILabel]()
    
    @IBAction func imageButton2(sender: UIButton) {
        image1.selected = false
        image1.layer.borderColor = clear.CGColor
        image1.layer.cornerRadius = 0
        image1.layer.borderWidth = 0
        
        image2.selected = true
        image2.layer.borderColor = blue.CGColor
        image2.layer.cornerRadius = 0
        image2.layer.borderWidth = 15.0
        
        image3.selected = false
        image3.layer.borderColor = clear.CGColor
        image3.layer.cornerRadius = 0
        image3.layer.borderWidth = 0
        
        image4.selected = false
        image4.layer.borderColor = clear.CGColor
        image4.layer.cornerRadius = 0
        image4.layer.borderWidth = 0
}
    
    @IBAction func imageButton4(sender: UIButton) {
        image1.selected = false
        image1.layer.borderColor = clear.CGColor
        image1.layer.cornerRadius = 0
        image1.layer.borderWidth = 0
        
        image2.selected = false
        image2.layer.borderColor = clear.CGColor
        image2.layer.cornerRadius = 0
        image2.layer.borderWidth = 0
        
        image3.selected = false
        image3.layer.borderColor = clear.CGColor
        image3.layer.cornerRadius = 0
        image3.layer.borderWidth = 0
        
        image4.selected = true
        image4.layer.borderColor = blue.CGColor
        image4.layer.cornerRadius = 0
        image4.layer.borderWidth = 15.0
        
    }
    
    @IBAction func imageButton3(sender: UIButton) {
        image1.selected = false
        image1.layer.borderColor = clear.CGColor
        image1.layer.cornerRadius = 0
        image1.layer.borderWidth = 0
        
        image2.selected = false
        image2.layer.borderColor = clear.CGColor
        image2.layer.cornerRadius = 0
        image2.layer.borderWidth = 0
        
        image3.selected = true
        image3.layer.borderColor = blue.CGColor
        image3.layer.cornerRadius = 0
        image3.layer.borderWidth = 15.0
        
        image4.selected = false
        image4.layer.borderColor = clear.CGColor
        image4.layer.cornerRadius = 0
        image4.layer.borderWidth = 0
        
    }
    
    @IBAction func imageButton1(sender: UIButton) {
        image1.selected = true
        image1.layer.borderColor = blue.CGColor
        image1.layer.cornerRadius = 0
        image1.layer.borderWidth = 15.0
        
        image2.selected = false
        image2.layer.borderColor = clear.CGColor
        image2.layer.cornerRadius = 0
        image2.layer.borderWidth = 0
        
        image3.selected = false
        image3.layer.borderColor = clear.CGColor
        image3.layer.cornerRadius = 0
        image3.layer.borderWidth = 0
        
        image4.selected = false
        image4.layer.borderColor = clear.CGColor
        image4.layer.cornerRadius = 0
        image4.layer.borderWidth = 0
        
    }
    
    @IBAction func optionButton5(sender: UIButton) {
        possibility5.hidden = true
        answerArray[matchOrder].text = answerArray[matchOrder].text! + " - " + possibility5.titleLabel!.text!
        matchOrder = matchOrder + 1
    }
    
    @IBAction func optionButton4(sender: UIButton) {
        possibility4.hidden = true
        answerArray[matchOrder].text = answerArray[matchOrder].text! + " - " + possibility4.titleLabel!.text!
        matchOrder = matchOrder + 1
    }
    
    @IBAction func optionButton3(sender: UIButton) {
        possibility3.hidden = true
        answerArray[matchOrder].text = answerArray[matchOrder].text! + " - " + possibility3.titleLabel!.text!
        matchOrder = matchOrder + 1
    }
    
    @IBAction func optionButton2(sender: UIButton) {
        possibility2.hidden = true
        answerArray[matchOrder].text = answerArray[matchOrder].text! + " - " + possibility2.titleLabel!.text!
        matchOrder = matchOrder + 1
    }
    
    @IBAction func optionButton1(sender: UIButton) {
        possibility1.hidden = true
        answerArray[matchOrder].text = answerArray[matchOrder].text! + " - " + possibility1.titleLabel!.text!
        matchOrder = matchOrder + 1
    }

    @IBAction func resetQuestion(sender: UIButton) {
        possibility1.hidden = false
        possibility2.hidden = false
        possibility3.hidden = false
        if (json[x][currentQuestionNumber-1]["answers"].count == 4) {
            possibility4.hidden = false
        } else if (json[x][currentQuestionNumber-1]["answers"].count == 5) {
            possibility4.hidden = false
            possibility5.hidden = false
        }
        
        
        
        answer1.text = String(json[x][currentQuestionNumber-1]["answers"][0])
        answer2.text = String(json[x][currentQuestionNumber-1]["answers"][1])
        answer3.text = String(json[x][currentQuestionNumber-1]["answers"][2])
        answer4.text = String(json[x][currentQuestionNumber-1]["answers"][3])
        answer5.text = String(json[x][currentQuestionNumber-1]["answers"][4])
        
        explanation.text = ""
        matchOrder = 0
    }
    
    func buildQuiz(x: String, y: NSInteger, json: JSON) {
        if (currentQuestionNumber <= sectionLength) {
            if (json[x][y]["type"] == 1 || json[x][y]["type"] == 2){
                answer4.hidden = false
                button4.hidden = false
                button4.selected = false
                answer5.hidden = false
                button5.hidden = false
                button5.selected = false
                resetButton.hidden = true
                answer1.hidden = false
                answer2.hidden = false
                answer3.hidden = false
                button1.hidden = false
                button2.hidden = false
                button3.hidden = false
                button6.hidden = false
                answer6.hidden = false
                button6.selected = false
                
                image1.hidden = true
                image2.hidden = true
                image3.hidden = true
                image4.hidden = true
                
                possibility1.hidden = true
                possibility2.hidden = true
                possibility3.hidden = true
                possibility4.hidden = true
                possibility5.hidden = true

                question.text = String(json[x][y]["question"])
                answer1.text = String(json[x][y]["answers"][0])
                answer2.text = String(json[x][y]["answers"][1])
                answer3.text = String(json[x][y]["answers"][2])
                if (json[x][y]["answers"].count == 4) {
                    answer4.text = String(json[x][y]["answers"][3])
                    answer5.hidden = true
                    button5.hidden = true
                    answer6.hidden = true
                    button6.hidden = true
                } else if (json[x][y]["answers"].count == 5) {
                    answer4.text = String(json[x][y]["answers"][3])
                    answer5.text = String(json[x][y]["answers"][4])
                    button5.hidden = false
                    answer6.hidden = true
                    button6.hidden = true
                } else if (json[x][y]["answers"].count == 6) {
                    answer4.text = String(json[x][y]["answers"][3])
                    answer5.text = String(json[x][y]["answers"][4])
                    button5.hidden = false
                    answer6.text = String(json[x][y]["answers"][5])
                    button6.hidden = false
                } else {
                    answer4.hidden = true
                    button4.hidden = true
                    answer5.hidden = true
                    button5.hidden = true
                    answer6.hidden = true
                    button6.hidden = true
                }
                explanation.text = ""
            } else if (json[x][y]["type"] == 3) {
                answer4.hidden = false
                button4.hidden = true
                button4.selected = false
                answer5.hidden = false
                button5.hidden = true
                button5.selected = false
                button3.hidden = true
                button3.selected = false
                button2.hidden = true
                button2.selected = false
                button1.hidden = true
                button1.selected = false
                resetButton.hidden = false
                button6.hidden = true
                answer6.hidden = true
                
                possibility1.hidden = false
                possibility2.hidden = false
                possibility3.hidden = false
                possibility4.hidden = false
                possibility5.hidden = false
                
                image1.hidden = true
                image2.hidden = true
                image3.hidden = true
                image4.hidden = true
                
                question.text = String(json[x][y]["question"])
                answer1.text = String(json[x][y]["answers"][0])
                answer2.text = String(json[x][y]["answers"][1])
                answer3.text = String(json[x][y]["answers"][2])
                
                answerArray.append(answer1)
                answerArray.append(answer2)
                answerArray.append(answer3)
                
                possibility1.setTitle(String(json[x][y]["possibilities"][0]), forState: UIControlState.Normal)
                possibility2.setTitle(String(json[x][y]["possibilities"][1]), forState: UIControlState.Normal)
                possibility3.setTitle(String(json[x][y]["possibilities"][2]), forState: UIControlState.Normal)
                
                if (json[x][y]["answers"].count == 4) {
                    answer4.text = String(json[x][y]["answers"][3])
                    possibility4.setTitle(String(json[x][y]["possibilities"][3]), forState: UIControlState.Normal)
                    answer5.hidden = true
                    possibility5.hidden = true
                    answerArray.append(answer4)
                } else if (json[x][y]["answers"].count == 5) {
                    answer4.text = String(json[x][y]["answers"][3])
                    possibility4.setTitle(String(json[x][y]["possibilities"][3]), forState: UIControlState.Normal)
                    answerArray.append(answer4)
                    answer5.text = String(json[x][y]["answers"][4])
                    possibility5.setTitle(String(json[x][y]["possibilities"][4]), forState: UIControlState.Normal)
                    answerArray.append(answer5)
                } else {
                    answer4.hidden = true
                    possibility4.hidden = true
                    answer5.hidden = true
                    possibility5.hidden = true
                }
                explanation.text = ""
            } else if (json[x][y]["type"] == 4) {
                answer4.hidden = true
                button4.hidden = true
                button4.selected = false
                answer5.hidden = true
                button5.hidden = true
                button5.selected = false
                answer3.hidden = true
                button3.hidden = true
                button3.selected = false
                answer2.hidden = true
                button2.hidden = true
                button2.selected = false
                answer1.hidden = true
                button1.hidden = true
                button1.selected = false
                resetButton.hidden = true
                button6.hidden = true
                answer6.hidden = true
                
                possibility1.hidden = true
                possibility2.hidden = true
                possibility3.hidden = true
                possibility4.hidden = true
                possibility5.hidden = true
                
                question.text = String(json[x][y]["question"])
                
                image1.hidden = false
                image2.hidden = false
                image3.hidden = false
                image4.hidden = false
                
                image1.setTitle(String(json[x][y]["answers"][0]), forState: UIControlState.Normal)
                image2.setTitle(String(json[x][y]["answers"][1]), forState: UIControlState.Normal)
                image3.setTitle(String(json[x][y]["answers"][2]), forState: UIControlState.Normal)
                image4.setTitle(String(json[x][y]["answers"][3]), forState: UIControlState.Normal)

                
                explanation.text = ""
            }

        } else {
            if (x == "6"){
                quizCompleted = true
            }
            quizReturn = true
            closeModal()
        }
    }
    
    func closeModal() {
        self.dismissViewControllerAnimated(true, completion: {})
    }
    
    @IBAction func submit(sender: UIButton) {
        checkAnswer()
    }
    
    @IBAction func option6(sender: UIButton) {
        if (json[x][currentQuestionNumber-1]["type"] == 2) {
            if (button6.selected == true) {
                button6.selected = false
                button6.backgroundColor = UIColor.whiteColor()
            } else {
                button6.selected = true
                button6.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
            }
            
        } else {
            button1.selected = false
            button1.backgroundColor = UIColor.whiteColor()
            button2.selected = false
            button2.backgroundColor = UIColor.whiteColor()
            button3.selected = false
            button3.backgroundColor = UIColor.whiteColor()
            button4.selected = false
            button4.backgroundColor = UIColor.whiteColor()
            button5.selected = false
            button5.backgroundColor = UIColor.whiteColor()
            button6.selected = true
            button6.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
        }

    }

    
    @IBAction func option5(sender: UIButton) {
        if (json[x][currentQuestionNumber-1]["type"] == 2) {
            if (button5.selected == true) {
                button5.selected = false
                button5.backgroundColor = UIColor.whiteColor()
            } else {
                button5.selected = true
                button5.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
            }
            
        } else {
        button1.selected = false
        button1.backgroundColor = UIColor.whiteColor()
        button2.selected = false
        button2.backgroundColor = UIColor.whiteColor()
        button3.selected = false
        button3.backgroundColor = UIColor.whiteColor()
        button4.selected = false
        button4.backgroundColor = UIColor.whiteColor()
        button5.selected = true
        button5.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
        button6.selected = false
        button6.backgroundColor = UIColor.whiteColor()
        }
    }
    
    @IBAction func option4(sender: UIButton) {
        if (json[x][currentQuestionNumber-1]["type"] == 2) {
            if (button4.selected == true) {
                button4.selected = false
                button4.backgroundColor = UIColor.whiteColor()
            } else {
                button4.selected = true
                button4.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
            }
            
        } else {
        button1.selected = false
        button1.backgroundColor = UIColor.whiteColor()
        button2.selected = false
        button2.backgroundColor = UIColor.whiteColor()
        button3.selected = false
        button3.backgroundColor = UIColor.whiteColor()
        button4.selected = true
        button4.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
        button5.selected = false
        button5.backgroundColor = UIColor.whiteColor()
            button6.selected = false
            button6.backgroundColor = UIColor.whiteColor()
            }
    }
    
    @IBAction func option3(sender: UIButton) {
        if (json[x][currentQuestionNumber-1]["type"] == 2) {
            if (button3.selected == true) {
                button3.selected = false
                button3.backgroundColor = UIColor.whiteColor()
            } else {
                button3.selected = true
                button3.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
            }
            
        } else {
        button1.selected = false
        button1.backgroundColor = UIColor.whiteColor()
        button2.selected = false
        button2.backgroundColor = UIColor.whiteColor()
        button3.selected = true
        button3.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
        button4.selected = false
        button4.backgroundColor = UIColor.whiteColor()
        button5.selected = false
        button5.backgroundColor = UIColor.whiteColor()
            button6.selected = false
            button6.backgroundColor = UIColor.whiteColor()
            }
    }
    
    @IBAction func option2(sender: UIButton) {
        if (json[x][currentQuestionNumber-1]["type"] == 2) {
            if (button2.selected == true) {
                button2.selected = false
                button2.backgroundColor = UIColor.whiteColor()
            } else {
                button2.selected = true
                button2.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
            }
            
        } else {
        button1.selected = false
        button1.backgroundColor = UIColor.whiteColor()
        button2.selected = true
        button2.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
        button3.selected = false
        button3.backgroundColor = UIColor.whiteColor()
        button4.selected = false
        button4.backgroundColor = UIColor.whiteColor()
        button5.selected = false
        button5.backgroundColor = UIColor.whiteColor()
            button6.selected = false
            button6.backgroundColor = UIColor.whiteColor()
            }
    }
    
    @IBAction func option1(sender: UIButton) {
        if (json[x][currentQuestionNumber-1]["type"] == 2) {
            if (button1.selected == true) {
                button1.selected = false
                button1.backgroundColor = UIColor.whiteColor()
            } else {
                button1.selected = true
                button1.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
            }
            
        } else {
        button1.selected = true
        button1.backgroundColor = UIColor(red: 56/255, green: 121/255, blue: 153/255, alpha: 1.0)
        button2.selected = false
        button2.backgroundColor = UIColor.whiteColor()
        button3.selected = false
        button3.backgroundColor = UIColor.whiteColor()
        button4.selected = false
        button4.backgroundColor = UIColor.whiteColor()
        button5.selected = false
        button5.backgroundColor = UIColor.whiteColor()
            button6.selected = false
            button6.backgroundColor = UIColor.whiteColor()
            }
    }
    
    @IBAction func clickNext(sender: UIButton) {
        resetInterface()
        currentQuestionNumber = currentQuestionNumber + 1
        submitButton.hidden = false
        nextButton.hidden = true
        submitButton.enabled = true
        buildQuiz(x, y: currentQuestionNumber-1, json: json)
        
    }
    
    func resetInterface() {
        button1.selected = false
        button1.backgroundColor = UIColor.whiteColor()
        button2.selected = false
        button2.backgroundColor = UIColor.whiteColor()
        button3.selected = false
        button3.backgroundColor = UIColor.whiteColor()
        button4.selected = false
        button4.backgroundColor = UIColor.whiteColor()
        button5.selected = false
        button5.backgroundColor = UIColor.whiteColor()
        button6.selected = false
        button6.backgroundColor = UIColor.whiteColor()
        //explanation.text = ""
    }
    
    func changeQuestion() {
        print("Next question please!")
    }
    
    func correctAnswerGiven() {
        if (currentQuestionNumber+1 <= sectionLength) {
        explanation.text = "Correct! Click Next to go to the next question."
        } else {
        explanation.text = "Correct! Click Next to return to the tour."
        }
        totalCorrectAnswer = totalCorrectAnswer + 1
        print("Correct answers so far: " + String(totalCorrectAnswer))
        goToNext()
    }
    
    var incorrect = 0
    
    func incorrectAnswerGiven() {
        if (incorrect == 0) {
        explanation.text = "Incorrect. Please Try again."
            incorrect = incorrect + 1
            resetInterface()
        } else if (incorrect == 1){
            explanation.text = "Incorrect: " + String(currentQuestion[x][currentQuestionNumber-1]["incorrect_explanation"])
            goToNext()
            
        }
    }
    
    func goToNext() {
        incorrect = 0
        submitButton.hidden = true
        nextButton.hidden = false
        submitButton.enabled = false
    }
    
    func checkAnswer() {
        if (currentQuestion[x][currentQuestionNumber-1]["type"] == 1){
        switch String(currentQuestion[x][currentQuestionNumber-1]["correct_answer"]) {
            case answer1.text! where button1.selected == true:
            correctAnswerGiven()
            case answer2.text! where button2.selected == true:
            correctAnswerGiven()
            case answer3.text! where button3.selected == true:
            correctAnswerGiven()
            case answer4.text! where button4.selected == true:
            correctAnswerGiven()
            case answer5.text! where button5.selected == true:
            correctAnswerGiven()
            case answer6.text! where button6.selected == true:
            correctAnswerGiven()
            default:
            incorrectAnswerGiven()
        }
        } else if (currentQuestion[x][currentQuestionNumber-1]["type"] == 2) {
            var temp = 0
            var answer = [Bool]()
            var buttons = [button1,button2,button3,button4,button5,button6]
            for _ in currentQuestion[x][currentQuestionNumber-1]["correct_answer"]{
                if (buttons[temp].selected == true && currentQuestion[x][currentQuestionNumber-1]["correct_answer"][temp] == false) {
                     answer.append(false)
                } else if (buttons[temp].selected == false && currentQuestion[x][currentQuestionNumber-1]["correct_answer"][temp] == true) {
                     answer.append(false)
                } else {
                    answer.append(true)
                }
                temp += 1
            }
            let correct = checkFalse(answer)
            if (correct == true) {
                correctAnswerGiven()
            } else {
                incorrectAnswerGiven()
            }

        } else if (currentQuestion[x][currentQuestionNumber-1]["type"] == 3) {
            var temp = 0
            var answer = [Bool]()
            var buttons = [answer1,answer2,answer3,answer4,answer5]
            for _ in currentQuestion[x][currentQuestionNumber-1]["correct_answer"]{
                if (buttons[temp].text == String(currentQuestion[x][currentQuestionNumber-1]["correct_answer"][temp])){
                    answer.append(true)
                } else {
                    answer.append(false)
                }
                temp += 1
            }
            
            let correct = checkFalse(answer)
            if (correct == true) {
                correctAnswerGiven()
            } else {
                incorrectAnswerGiven()
            }
            
        } else if (currentQuestion[x][currentQuestionNumber-1]["type"] == 4) {
            switch String(currentQuestion[x][currentQuestionNumber-1]["correct_answer"]) {
            case image1.titleLabel!.text! where image1.selected == true:
                correctAnswerGiven()
            case image2.titleLabel!.text! where image2.selected == true:
                correctAnswerGiven()
            case image3.titleLabel!.text! where image3.selected == true:
                correctAnswerGiven()
            case image4.titleLabel!.text! where image4.selected == true:
                correctAnswerGiven()
            default:
                incorrectAnswerGiven()
            }

        }
    }
    
    func checkFalse(answer: Array<Bool>) -> Bool {
        for a in answer {
            if (a == false) {
                return false
            }
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
