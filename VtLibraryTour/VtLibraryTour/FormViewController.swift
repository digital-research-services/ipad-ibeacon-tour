//
//  FormViewController.swift
//  VtLibraryTour
//
//  Created by Jonathan Bradley on 9/6/16.
//  Copyright © 2016 jonathanbradley@vt.edu. All rights reserved.
//

import UIKit
import Alamofire

class FormViewController: UIViewController {
    @IBOutlet weak var iName: UITextField!
    @IBOutlet weak var course: UITextField!
    @IBOutlet weak var lName: UITextField!
    @IBOutlet weak var fName: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func formSubmit(sender: UIButton) {
        
        let parameters = [
            "FirstName": fName.text! as String,
            "LastName": lName.text! as String,
            "Instructor": iName.text! as String,
            "Course": course.text! as String,
            "Grade": String(totalCorrectAnswer)
        ]
        
        Alamofire.request(.POST, "https://script.google.com/macros/s/AKfycbxcR2oaAb2Z16z7jRKB2RKmQKxtHWsWidPA6mbSnZ2gViA2vVY/exec", parameters: parameters)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
