//
//  TourStop.swift
//  VtLibraryTour
//
//  Created by Keith Gilbertson on 9/2/15.
//  Copyright (c) 2015 jonathanbradley@vt.edu. All rights reserved.
//

import Foundation


// This represents a stop on the tour
struct TourStop  {
    
    let major: CLBeaconMajorValue
    let minor: CLBeaconMinorValue
    let identifier: String
    let videoName: String // filename of video, including extension
    
}


// Add and order new tour stops in this tourBeacons Array
let tourBeacons = [

    // Beacon 1 Light Green
    TourStop(major: 24214, minor: 63237, identifier: "Circulation", videoName: "TourStop1.mp4"),
    // Beacon 2 Light Blue
    TourStop(major: 40721, minor: 49816, identifier: "Reference Desk", videoName: "TourStop2.mp4"),
    // Beacon 3 Purple
    TourStop(major: 51879, minor: 16989, identifier: "Writing Center", videoName: "TourStop3.mp4"),
    // Beacon 4 Light Blue
    TourStop(major: 24256, minor: 39335, identifier: "Special Collections", videoName: "TourStop4.mp4"),
    // Beacon 5 Light Green
    TourStop(major: 34147, minor: 25022, identifier: "Group Study Rooms", videoName: "TourStop5.mp4"),
    // Beacon 6 Purple
    TourStop(major: 2832, minor: 3651, identifier: "Library Stacks", videoName: "TourStop6.mp4"),
    // Beacon 7 Purple
    //TourStop(major: 2832, minor: 3651, identifier: "temp_name_7", videoName: "04_readingroom"),
    // Beacon 8 Light Green
    TourStop(major: 5928, minor: 36186, identifier: "Finale (Go to the main elevators on the 3rd floor)", videoName: "TourFinale.mp4"),
    // Beacon 9 ?
    //TourStop(major: ####, minor: #####, identifier: "temp_name_9", videoName: "09_refdesk"),
    // Beacon 10 ?
    //TourStop(major: ####, minor: #####, identifier: "temp_name_10", videoName: "04_readingroom"),
    // Beacon 11 ?
    //TourStop(major: ####, minor: #####, identifier: "temp_name_11", videoName: "05_spec"),,

]

// Sets the base url for the files to be downloaded
let baseUrl = "https://googledrive.com/host/0B2yjdSMrlpVMQ29mQmpiTzNzakk/"

let UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D"

var totalCorrectAnswer = 0

