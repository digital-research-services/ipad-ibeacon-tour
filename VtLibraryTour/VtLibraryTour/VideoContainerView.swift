//
//  VideoContainerView.swift
//  VtLibraryTour
//
//  Created by Jonathan Bradley on 6/21/16.
//  Copyright © 2016 jonathanbradley@vt.edu. All rights reserved.
//

import UIKit

let segueIdentifierVideo = "embedVideo"
let segueIdentifierImage = "embedImage"
var currentSegue = ""

class VideoContainerView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        currentSegue = segueIdentifierImage
        self.performSegueWithIdentifier(currentSegue, sender: nil)
        // Do any additional setup after loading the view.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(VideoContainerView.swapViewControllers), name:"switchControllers", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == segueIdentifierImage) {
            if self.childViewControllers.count > 0 {
                swapFromViewController(self.childViewControllers[0], toViewController: segue.destinationViewController)
            }
            else {
                self.addChildViewController(segue.destinationViewController)
                ((segue.destinationViewController as! UIViewController)).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
                self.view!.addSubview(((segue.destinationViewController as! UIViewController)).view!)
                segue.destinationViewController.didMoveToParentViewController(self)
            }
        }
        else if (segue.identifier == segueIdentifierVideo) {
            swapFromViewController(self.childViewControllers[0], toViewController: segue.destinationViewController)
        }
        
    }
    
    func swapFromViewController(fromViewController: UIViewController, toViewController: UIViewController) {
        toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        fromViewController.willMoveToParentViewController(nil)
        self.addChildViewController(toViewController)
        self.transitionFromViewController(fromViewController, toViewController: toViewController, duration: 1.0, options: .TransitionCrossDissolve, animations: nil, completion: {(finished: Bool) -> Void in
            fromViewController.removeFromParentViewController()
            toViewController.didMoveToParentViewController(self)
        })
    }
    
    func swapViewControllers() {
        currentSegue = (currentSegue == segueIdentifierImage) ? segueIdentifierVideo : segueIdentifierImage
        self.performSegueWithIdentifier(currentSegue, sender: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
