//
//  PageContentViewController.swift
//  VtLibraryTour
//
//  Created by Jonathan Bradley on 6/17/16.
//  Copyright © 2016 jonathanbradley@vt.edu. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

var videoPlayed = [Bool](count: numberOfTourStops, repeatedValue: false)
var currentStop = 0
let avPlayerViewController = AVPlayerViewController()
var tourCompleted = false
var quizCompleted = false
let videoURLUpdate = "com.jonathanbradley.videoNotification"
let switchImage = "com.jonathanbradley.switchImageNotification"
let jsonDirectoryURL = NSURL(fileURLWithPath: NSTemporaryDirectory())
let jsonLocalPath = jsonDirectoryURL.URLByAppendingPathComponent("ipad_tour_questions.json")
let quizTrigger1 = 2;
let quizTrigger2 = 4;
let quizTrigger3 = 6;
var coverController: CoverImageController?
var quizReturn = false
var doneWatching = false
// Validates that the user is at the correct stop, and returns

// stop information if true

class PageContentViewController: UIViewController, ESTBeaconManagerDelegate {
    
    let beaconManager = ESTBeaconManager()
    @IBOutlet weak var nextButton: UIButton!
    var beaconRegions = [CLBeaconRegion]()
    var activeStop = ""
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var infoBox: UILabel!
    @IBOutlet weak var progressBar: UIPageControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressBar.numberOfPages = numberOfTourStops
        
        infoBox.text = "Welcome to the tour! Head to your first destination: " + tourBeacons[currentStop].identifier
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PageContentViewController.searchForBeacons), name:"searchForBeacons", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PageContentViewController.waitForRewatch), name:"videoFinished", object: nil)

        // initialize regions
        for beacon in tourBeacons  {
            
            let beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: UUID)!, major: beacon.major, minor: beacon.minor, identifier: beacon.identifier)
            beaconRegions.append(beaconRegion)
          //  print("Initialized region for \(beacon.identifier)")
            
        }
        
        self.beaconManager.delegate = self
        self.beaconManager.returnAllRangedBeaconsAtOnce = true
        
        self.beaconManager.requestWhenInUseAuthorization()
        
        startBeacons()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if quizReturn {
            var activeStop = tourBeacons[currentStop-1].videoName
            var stopDict = [ "activeStop": activeStop]
            NSNotificationCenter.defaultCenter().postNotificationName(videoURLUpdate, object: stopDict )
            swapButtonPressed(playButton)
            quizReturn = false
        }
    }
    
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        // stop listening for Beacons when this screen is no
        // longer displayed
        stopBeacons()
    }
    
    func initiateQuiz(stop: NSInteger){
        self.performSegueWithIdentifier("quizSegue", sender: nil)        //self.presentViewController(QuizViewController(), animated: true, completion: nil)
    }
    
    @IBAction func nextQuiz(sender: UIButton) {
        if (doneWatching == true) {
        initiateQuiz(currentStop)
        nextButton.hidden = true
        infoBox.text = ""
        } else {
            doneWatching = true
            NSNotificationCenter.defaultCenter().postNotificationName(switchImage, object: nil)
            nextButton.hidden = true
            infoBox.text = ""
            NSNotificationCenter.defaultCenter().postNotificationName("switchControllers", object: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("searchForBeacons", object: nil)
        }
    }
    
    func startBeacons() {
        for region in beaconRegions  {
            self.beaconManager.startRangingBeaconsInRegion(region)
            print("Ranging beacons in \(region)")
        }
    }
    
    func stopBeacons() {
        for region in beaconRegions  {
            self.beaconManager.stopRangingBeaconsInRegion(region)
            // print("stopped listening for beacons in region \(region)")
        }
    }
    
    func searchForBeacons(){
        startBeacons()
        if (currentStop < numberOfTourStops) {
            infoBox.text = "Head to your next destination: " + tourBeacons[currentStop].identifier
            progressBar.currentPage += 1
        }
    }
    
    func waitForRewatch() {
        nextButton.hidden = false
        doneWatching = false
        infoBox.text = "You can rewatch the video, or tap NEXT to continue the tour"
    }
    
    func beaconManager(manager: AnyObject!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {
        
        
        // find out tourBeacons object and play appropriate video
        
        if let nearestBeacon = beacons.first as? CLBeacon {
            
           // print("nearestBeacon = \(nearestBeacon)")
            if let stop = validateTourStop(nearestBeacon)  {
                print("Tour Stop validated, sending notification.")
                for region in beaconRegions  {
                    beaconManager.stopRangingBeaconsInRegion(region)
                    print("stopped listening for beacons in region \(region)")
                }
                if (currentStop - 1 == quizTrigger1 || currentStop - 1  == quizTrigger2 || currentStop - 1 == quizTrigger3){
                infoBox.text = "You've reached the destination, but before we continue the tour, let's review what we've learned so far. Click the Next button to start the quiz."
                enableQuizNext()
                NSNotificationCenter.defaultCenter().postNotificationName(switchImage, object: nil)
                } else {
                    activeStop = stop.videoName
                    print(activeStop)
                    var stopDict = [ "activeStop": stop.videoName]
                    NSNotificationCenter.defaultCenter().postNotificationName(videoURLUpdate, object: stopDict )
                    enableButton()
                    infoBox.text = "You've found the next stop! Push play when you are ready to watch the next video."
                }
            }
            
        } else {
            // no beacons found
            
        }
        
    }
    
    func beaconManager(manager: AnyObject!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .Denied || status == .Restricted {
            NSLog("Location Services authorization denied, can't range")
        }
    }
    
    func beaconManager(manager: AnyObject!, rangingBeaconsDidFailForRegion region: CLBeaconRegion!, withError error: NSError!) {
        NSLog("Ranging beacons failed for region '%@'\n\nMake sure that Bluetooth and Location Services are on, and that Location Services are allowed for this app. Also note that iOS simulator doesn't support Bluetooth.\n\nThe error was: %@", region.identifier, error);
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var containerViewController : VideoContainerView!
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "embedContainer") {
            containerViewController = segue.destinationViewController as! VideoContainerView
        }
    }
    
    @IBAction func swapButtonPressed(sender: AnyObject) {
        containerViewController.swapViewControllers()
        playButton.hidden = true
        infoBox.text = ""
    }
    
    func enableButton() {
        playButton.hidden = false
        NSNotificationCenter.defaultCenter().postNotificationName(switchImage, object: nil)
    }
    
    func enableQuizNext() {
        nextButton.hidden = false
    }
    
    func callCompleteView() {
        if (tourCompleted == true && quizCompleted == true) {
            self.performSegueWithIdentifier("completionSegue", sender: nil)
            print("Complete View Called")
        } else {
            print("Not complete yet")
        }
    }
    
    func validateTourStop(beacon: CLBeacon)  -> TourStop?  {
        
        if currentStop < numberOfTourStops { //check for end of tour to make sure the array doesn't throw a fatal error once all videos are played.
            let correctStop = tourBeacons[currentStop]
            
            if beacon.major.unsignedShortValue == correctStop.major &&
                beacon.minor.unsignedShortValue == correctStop.minor
            {
                //print("Found stop\(tourBeacons[currentStop])")
                return tourBeacons[currentStop++]
            }
            
        } else {
            if (tourCompleted==false) {
                print("Tour Complete") //This else statement will be where we call the fuction for the "end" of the tour. Probably a quiz, though it could be a set of instructions for returning the device.
                tourCompleted = true
                callCompleteView()
                
            }
        }
        return nil
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


}