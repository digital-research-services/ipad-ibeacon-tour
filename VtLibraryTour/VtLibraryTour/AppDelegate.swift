//
//  AppDelegate.swift
//  VtLibraryTour
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var moviePath: NSURL!
    let videoFinishedPlaying = "com.jonathanbradley.videoCompleteNotification"

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        ESTCloudManager.setupAppID("vt-library-tour", andAppToken: "55151a3c7c0718890436b6604dbc852e")
        print("Video Player View Loaded")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.addURL(_:)), name: videoURLUpdate, object: nil)
        
        return true
    }
    

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func addURL(notification: NSNotification){
        print("Notification Received")
        var fileName = notification.object as! NSDictionary
        var activeStopNumber = fileName["activeStop"]
        
        let directoryURL = NSURL(fileURLWithPath: NSTemporaryDirectory()) // get ~/Library directory for this app
        moviePath = directoryURL.URLByAppendingPathComponent(activeStopNumber as! String) // appends filename for this stop to the end of the path
    }

}

