//
//  UpdateViewController.swift
//  VtLibraryTour
//
//  Created by Jonathan Bradley on 8/31/16.
//  Copyright © 2016 jonathanbradley@vt.edu. All rights reserved.
//

import UIKit
import Alamofire

let numberOfTourStops = tourBeacons.count

extension CALayer {
    var borderUIColor: UIColor {
        set {
            self.borderColor = newValue.CGColor
        }
        get {
            return UIColor(CGColor: self.borderColor!)
        }
    }
}

class UpdateViewController: UIViewController {

    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var text: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.hidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func updatePush(sender: UIButton) {
        
        var downloadCount = 0
        loadingIndicator.hidden = false
        skipButton.hidden = true
        updateButton.hidden = true
        text.hidden = true
        var jsonLocalPath: NSURL?
        let jsonURL = baseUrl + "ipad_tour_questions.json"
//        var finaleLocalPath: NSURL?
//        let finaleURL = baseUrl + "TourFinale.mp4"
        
        clearTmpDirectory()
        
        Alamofire.download(.GET, jsonURL, destination: { (temporaryURL, response) in //does an http request and grabs the file as specified url, saves it
            let directoryURL = NSURL(fileURLWithPath: NSTemporaryDirectory()) //gets the ~/Library directory for this app
            let pathComponent = response.suggestedFilename //gets the suggested filename i.e. what the file was originally named before being downloaded
            
            jsonLocalPath = directoryURL.URLByAppendingPathComponent(pathComponent!) //appends the suggested filename to the Library path
            return jsonLocalPath!//returns the full path
            
        })
            .response { (request, response, _, error) in
                //print(response) //prints the http request response
                print("Downloaded json file to \(jsonLocalPath!)") // prints the file location, used for debugging
        }
        
//        Alamofire.download(.GET, finaleURL, destination: { (temporaryURL, response) in //does an http request and grabs the file as specified url, saves it
//            let directoryURL = NSURL(fileURLWithPath: NSTemporaryDirectory()) //gets the ~/Library directory for this app
//            let pathComponent = response.suggestedFilename //gets the suggested filename i.e. what the file was originally named before being downloaded
//            
//            finaleLocalPath = directoryURL.URLByAppendingPathComponent(pathComponent!) //appends the suggested filename to the Library path
//            return finaleLocalPath!//returns the full path
//            
//        })
//            .response { (request, response, _, error) in
//                print(response) //prints the http request response
//                print("Downloaded finale file to \(finaleLocalPath!)") // prints the file location, used for debugging
//        }
        
        // download updated files when the app loads; could be moved to an action triggered function if need be.
        for TourStop in tourBeacons {
            
            var localPath: NSURL?
            let fullUrl = baseUrl + TourStop.videoName
            
            Alamofire.download(.GET, fullUrl, destination: { (temporaryURL, response) in //does an http request and grabs the file as specified url, saves it
                let directoryURL = NSURL(fileURLWithPath: NSTemporaryDirectory()) //gets the ~/Library directory for this app
                let pathComponent = response.suggestedFilename //gets the suggested filename i.e. what the file was originally named before being downloaded
                
                localPath = directoryURL.URLByAppendingPathComponent(pathComponent!) //appends the suggested filename to the Library path
                return localPath!//returns the full path
                
            })
                .response { (request, response, _, error) in
                    //print(response) //prints the http request response
                    print("Downloaded file to \(localPath!)") // prints the file location, used for debugging
                    downloadCount = downloadCount + 1
                    if (downloadCount == numberOfTourStops) {
                        self.skipButton.hidden = false
                        self.skipButton.setTitle("NEXT",forState: UIControlState.Normal)
                        self.loadingIndicator.hidden = true
                        self.text.hidden = false
                        self.text.text = "Files updated."
                    }
            }
        }
        //end download files
        
    }
    
    func clearTmpDirectory() {
        do {
            let tmpDirectory = try NSFileManager.defaultManager().contentsOfDirectoryAtPath(NSTemporaryDirectory())
            try tmpDirectory.forEach { file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try NSFileManager.defaultManager().removeItemAtPath(path)
            }
        } catch {
            print(error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
