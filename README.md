# General Information 
This application is designed for iPads communicating with iBeacons posted in Newman Library. 

# Features / Requirements

* Compatible with [iPads 3rd generation and later](https://support.apple.com/en-us/HT201471), all iPad minis, and iPhone 4 and later
* Compatible with all generations of library-owned iBeacons 
* Compatible with library content (currently .mp4s) 
* Application loads new content automatically when user approaches a particular iBeacon
* Tour content can be updated periodically by librarians (say once per semester)
* Application asks for user input at every iBeacon stop, quizzing the students about the content
* Application provides correct answer if the student gets the question wrong, then send them to next stop
* Application acquires name and course from students who are required to take the tour
* Application records whether particular students have finished the tour and sends that information back to the instructor of the course (via Canvas?)

# Developer Notes

* Built with XCode 6.4 
* Written in Swift 1.2 

# iBeacon Notes

The library iBeacons are manufactured by [Estimote](http://estimote.com/). 

* Older iBeacons, Rev.D3.4, take battery model is CR2477 3-volt lithium coin batteries - the iBeacons have to be sliced open in order to replace the batteries
* Newer iBeacons take CR2450 3-volt lithium coin batteries



See also the [iPad Tour App Project folder](https://drive.google.com/a/vt.edu/folderview?id=0B9DTgpSH22wZflhMOFNyLXc2Q25zUGNVX2xuQnFweHBQRk0xMHV2UnBUZHZnQ0dQb2U1dkk&musp=sharing) for mockups and notes.